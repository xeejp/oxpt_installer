# oxpt_installer

Installer for Oxpt

## How to use

Please download the source first

### Normal activation

Double click START or Command line:

```sh
./START
```

### First time installation

**Warning: If the environment has already been built.
 This installer may destroy the existing environment.**

#### For macOS

Double click INSTALL_MAC or Command line: 

```sh
./INSTALL_MAC
```